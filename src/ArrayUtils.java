import java.util.Scanner;

public class ArrayUtils {

    /**
     * Função para preencher o Array
     */
    public static void preencher(int[] a) {
        Scanner ler = new Scanner(System.in);
        for (int i = 0; i < a.length; i++) {
            System.out.print("Entre com os valores na posicao solicitada A[" + i + "]: ");
            a[i] = ler.nextInt();
        }
    }

    /**
     * Função para imprimir o Array
     */
    public static void imprimir(int[] a) {
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }

    /**
     * Função de soma vai retornar a soma de todos os valores do Array
     */
    public static int somar(int[] a) {
        int soma = 0;
        for (int i = 0; i < a.length; i++) {
            soma = (soma + a[i]);
        }
        return soma;
        //return System.out.println(soma);
    }

    /**
     * Função buscar um elemento dentro de um Array
     */
    public static void buscarElemento(int[] a, int elementoProcurado) {

    }

    /**
     * Função para imprimir o Array na ordem inversa que foi digitado
     */
    public static void imprimirInverso(int[] a) {
        for (int i = a.length; i > a.length; i--) {
            System.out.println(a[i]);
        }
    }

    /**
     * Função que vai retornar a soma dos valores de dois Arrays
     */
    public static int somarDois_Elementos(int[] a, int[] b) {
        int soma = 0;
        for (int i = 0; i < a.length; i++) {
            soma = (a[i] + b[i]);
        }
        return soma;
    }
    
    public static void main(String[] args) {
        
        int[] arrayA = new int[5];
        int[] arrayB = new int[5];
        
        
    }

}
